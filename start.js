import log from 'npmlog';
import dotenv from 'dotenv';
import db from './src/models';
import server from './server';

dotenv.config();

const PORT = process.env.PORT || 4000;

server.listen(PORT, () => {
    log.info('user server is running on', PORT);
    db();
  });