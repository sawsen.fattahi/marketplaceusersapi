import express from 'express';
import bodyParser from 'body-parser';
import { usersRoutes } from './src/routes';

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
usersRoutes(app);

export default app;
