import {
  getUsers,
  addUser,
  signInUser,
  signOutUser,
  updateUser,
  activateAccount,
} from '../handlers';
import { authorization } from '../middleware';

const userRoutes = (app) => {
  app.route('/signup').post((req, res) => addUser(req, res));
  app.route('/signin').put((req, res) => signInUser(req, res));
  app.route('/signout').put(authorization, (req, res) => signOutUser(req, res));
  app.route('/users').get(authorization, (_, res) => getUsers(_, res));
  app.route('/users').put((req, res) => res.json({ message: 'accounts are updated' }));
  app.route('/user').get((req, res) => res.json({ message: 'user info' }));
  app.route('/user').delete(authorization, (req, res) => updateUser(req, res));
  app.route('/user').put(authorization, (req, res) => updateUser(req, res));
  app.route('/activate').put((req, res) => activateAccount(req, res));
};

export default userRoutes;
