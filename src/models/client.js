import mongodb from 'mongodb';
import dotenv from 'dotenv';

dotenv.config();
const HOST = process.env.DBHOST;
const PORT = process.env.DBPORT;
const NAME = process.env.DBNAME;

const { MongoClient } = mongodb;

const url = `mongodb://${HOST}:${PORT}/${NAME}`;
const client = async () => {
  const cl = await MongoClient.connect(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
  return cl;
};
export default client;
