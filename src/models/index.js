import dotenv from 'dotenv';
import log from 'npmlog';
import UserValidator from './userValidator';
import client from './client';

dotenv.config();

const NAME = process.env.DBNAME;

const connectMongo = async () => {
  const clt = await client();
  const dbo = clt.db(NAME);
  const collections = await dbo.listCollections().toArray();
  const dbHaseUser = collections.find((item) => item.name === 'User');
  const dbHasValidationToken = collections.find((item) => item.name === 'Validation_Token');
  if (dbHaseUser) {
    log.info('User collection already exists');
  } else {
    await dbo.createCollection('User', UserValidator, { strict: true });
    await dbo.collection('User').createIndex({ userLogin: 1 }, { unique: true });
    log.info('User collection was created!');
  }
  if (dbHasValidationToken) {
    log.info('Validation_Token collection already exists');
  } else {
    await dbo.createCollection('Validation_Token');
    log.info('Validation_Token collection was created!');
  }

  clt.close();
};

export default connectMongo;
