export default {
  validator: {
    $jsonSchema: {
      bsonType: 'object',
      required: ['userLogin', 'userPassword'],
      properties: {
        userLogin: {
          bsonType: 'string',
          pattern: '^.+@.+$',
          description: 'must be a string and match the regular expression pattern',
        },
        userPassword: {
          bsonType: 'string',
          description: 'must be a string and is required',
        },
        userStatus: {
          enum: [1, 2, 3, 4],
          description: 'can only be one of the enum values',
        },
        userAuth: {
          enum: [1, 2],
          description: 'can only be one of the enum values',
        },
      },
    },
  },
  validationAction: 'warn',
};
