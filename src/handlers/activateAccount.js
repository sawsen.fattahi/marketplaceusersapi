import activateAccountService from '../services/activateAccountService';

const activateAccount = async (req, res) => {
  const { token } = req.query;
  if (!token) {
    res.json({ status: 400, message: 'Token is missing' });
  } else {
    const response = await activateAccountService(token);
    res.json(response);
  }
};

export default activateAccount;
