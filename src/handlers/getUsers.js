import getUsersService from '../services/getUsersServices';

const getUsers = async (req, res) => {
  const { body } = req;
  const { filter } = body;
  const response = await getUsersService(filter);
  res.json(response);
};
export default getUsers;
