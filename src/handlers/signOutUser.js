import { signOutUserService } from '../services';

const signOutUser = async (req, res) => {
  const { body } = req;
  const { login } = body;
  const logout = await signOutUserService(login);
  res.json(logout);
};

export default signOutUser;
