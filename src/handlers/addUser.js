import bcrypt from 'bcrypt';
import dotenv from 'dotenv';
import addUserService from '../services/addUserService';

dotenv.config();

const { SALT_ROUNDS } = process.env;
const adduser = async (req, res) => {
  const { body } = req;
  const { login, password } = body;
  if (!login || !password) {
    res.json({ status: 300, message: 'some fields are missing!' });
  } else {
    const salt = await bcrypt.genSalt(parseInt(SALT_ROUNDS, 2));
    const hashedPassword = await bcrypt.hash(password, salt);
    const newUser = {
      userLogin: login,
      userPassword: hashedPassword,
      userAuth: 1,
      userStatus: 1,
      userCreatedDate: new Date().toISOString().replace(/T/, ' ').replace(/\..+/, ''),
    };
    const response = await addUserService(newUser);
    res.json(response);
  }
};

export default adduser;
