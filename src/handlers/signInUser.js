import validator from 'email-validator';
import bcrypt from 'bcrypt';
import { signInUserService } from '../services';

const signInUser = async (req, res) => {
  const { body } = req;
  const { login, password } = body;
  if (!login || !password) {
    res.json({ status: 300, message: 'some fields are missing!' });
  } else if (!validator.validate(login)) {
    res.json({ status: 300, message: 'email is not valid' });
  } else {
    const auth = await signInUserService(login, password);
    res.json(auth);
  }
};

export default signInUser;
