import { updateUserService } from '../services';

const updateUser = async (req, res) => {
  const { body } = req;
  const {id, data } = body;
  if (!id) {
    res.json({ status: 300, message: 'some fields are missing!' });
  }
  const logout = await updateUserService({ _id: id }, data);
  res.json(logout);
};

export default updateUser;
