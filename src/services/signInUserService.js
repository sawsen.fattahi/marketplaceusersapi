import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import dotenv from 'dotenv';
import client from '../models/client';
import updateUserService from './updateUserService';

dotenv.config();
const NAME = process.env.DBNAME;
const { TOKENSECRET } = process.env;

const signInUserService = async (userLogin, userPassword) => {
  const clt = await client();
  const dbo = clt.db(NAME);
  try {
    const user = await dbo.collection('User').findOne({ userLogin, userStatus: 3 });
    if (user) {
      const hashedPassword = user.userPassword;
      const isPassword = await bcrypt.compare(userPassword, hashedPassword);
      if (isPassword) {
        const { _id } = user;
        await updateUserService({ _id }, { userAuth: 2 });
        const token = await jwt.sign({ userId: _id }, TOKENSECRET);
        return { user, token, status: 200 };
      }
    }
    return { message: 'invalid login and/or password! ' };
  } catch (err) {
    return { message: err, status: 500 };
  }
};

export default signInUserService;
