import log from 'npmlog';
import dotenv from 'dotenv';
import client from '../models/client';

dotenv.config();
const NAME = process.env.DBNAME;

const getUsersService = async (filter) => {
  const clt = await client();
  const dbo = await clt.db(NAME);
  try {
    const response = await dbo.collection('User').find(filter);
    const res = await response.toArray();
    clt.close();
    log.info('list users: ', res);
    return { user: res, status: 200 };
  } catch (e) {
    log.info(e);
    return { message: e, status: 500 };
  }
};

export default getUsersService;
