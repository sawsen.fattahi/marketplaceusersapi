import log from 'npmlog';
import dotenv from 'dotenv';
import client from '../models/client';
import updateUserService from './updateUserService';

dotenv.config();
const NAME = process.env.DBNAME;

const activateAccountService = async (validationToken) => {
  const clt = await client();
  const dbo = clt.db(NAME);
  try {
    const filter = { token: validationToken };
    const response = await dbo.collection('Validation_Token').find(filter).toArray();
    const { _userId } = response[0];
    const res = await updateUserService({ _id: _userId }, { userStatus: 3 });
    await dbo.collection('Validation_Token').deleteOne(filter);
    return { user: res, status: 200 };
  } catch (e) {
    log.error(e);
    return { message: e, code: 500 };
  }
};

export default activateAccountService;
