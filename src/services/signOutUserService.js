import updateUserService from './updateUserService';

const signOutUserService = async (userLogin) => {
  try {
    await updateUserService({ userLogin }, { userAuth: 1 });
    return { message: 'user was disconnected', status: 200 };
  } catch (err) {
    return { message: err, status: 500 };
  }
};

export default signOutUserService;
