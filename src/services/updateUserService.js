import log from 'npmlog';
import dotenv from 'dotenv';
import client from '../models/client';

dotenv.config();
const NAME = process.env.DBNAME;

const updateUserService = async (query, update) => {
  const clt = await client();
  const dbo = clt.db(NAME);
  try {
    const response = await dbo.collection('User').findOneAndUpdate(query, { $set: update }, { returnOriginal: false });
    log.info('user updated: ', response);
    return { user: response, status: 200 };
  } catch (err) {
    log.error(err);
    return { message: err, status: 500 };
  }
};

export default updateUserService;
