import log from 'npmlog';
import dotenv from 'dotenv';
import client from '../models/client';
import validationTokenservice from './validationTokenService';

dotenv.config();
const NAME = process.env.DBNAME;

const addUserService = async (newUser) => {
  const clt = await client();
  const dbo = await clt.db(NAME);
  try {
    const response = await dbo.collection('User').insertOne(newUser);
    const { userLogin } = newUser;
    await validationTokenservice(userLogin);
    clt.close();
    log.info('user added: ', response);
    return { user: response, status: 200 };
  } catch (e) {
    log.error(e);
    if (e.code === 11000) {
      return { message: `user ${newUser.userLogin} already exists`, code: 1100 };
    }
    return { message: e, code: 500 };
  }
};

export default addUserService;
