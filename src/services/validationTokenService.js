import crypto from 'crypto';
import log from 'npmlog';
import dotenv from 'dotenv';
import client from '../models/client';
import getUsersService from './getUsersServices';

dotenv.config();
const NAME = process.env.DBNAME;

const validationTokenservice = async (login) => {
  const clt = await client();
  const dbo = clt.db(NAME);
  try {
    const filter = { userLogin: login };
    const res = await getUsersService(filter);
    const { user } = res;
    const { _id } = user[0];
    const validationToken = { _userId: _id, token: `${_id }${crypto.randomBytes(16).toString('hex')}`};
    const response = await dbo.collection('Validation_Token').insertOne(validationToken);
    clt.close();
    log.info('token added', response);
    return { token: response, status: 200 };
  } catch (e) {
    log.error(e);
    return { message: e, status: 500 };
  }
};

export default validationTokenservice;
