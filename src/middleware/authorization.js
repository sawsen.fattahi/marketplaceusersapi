import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';

dotenv.config();

const { TOKENSECRET } = process.env;
const auth = async (req, res, next) => {
  const { authorization } = req.headers;

  if (!authorization) {
    return res.status(401).send({ error: 'you must be logged in.' });
  }
  try {
    const token = await authorization.split(' ')[1];
    const payload = await jwt.verify(token, TOKENSECRET);
    const { userId } = payload;

    if (req.body.userId && req.body.userId !== userId) {
      return res.status(401).send({ error: 'invalid user id' });
    }
  } catch (error) {
    return res.status(401).send({ error });
  }
  return next();
};

export default auth;
