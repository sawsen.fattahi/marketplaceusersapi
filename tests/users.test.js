import chai from 'chai';
import chaiHttp from 'chai-http';
import app from '../server';

chai.use(chaiHttp);
chai.should();
// eslint-disable-next-line no-undef
describe('users', () => {
  // eslint-disable-next-line no-undef
  describe('GET /users', () => {
    // eslint-disable-next-line no-undef
    it('should returns authorization error', (done) => {
      chai.request(app)
        .get('/users')
        .end((err, res) => {
          res.should.have.status(401);
          res.body.should.be.a('object');
          res.body.should.be.eql({ error: 'you must be logged in.' });
          done();
        });
    });
  });
});